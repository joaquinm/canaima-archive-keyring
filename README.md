Repositorio Git del paquete canaima-archive-keyring
===================================================

En este repositorio se llevarán todas las actividades
relacionadas con el manejo de llaves públicas gpg
de los repositorios de paquetes de Canaima GNU/Linux.

Notas de interés
----------------

canaima-archive-keyring reemplaza al paquete histórico
canaima-llaves, utilizando la lógica base del paquete
debian-archive-keyring.

El Proyecto Debian hace disponible un conjunto de
herramientas y un flujo de trabajo para mantener el
ciclo de vida de las llaves gpg de su archivo. Aquí
se ha reimplementado el funcionamiento del paquete
debian-archive-keyring a fin de alcanzar el mismo
propósito, pero para las llaves gpg de los repositorios
del Proyecto Canaima.

No todas las rutinas dentro de esta lógica son
automatizables. El trabajo de mantenimiento de este
paquete exige ejecutar rutinas manuales antes del
proceso de construcción del paquete.

A la fecha de construcción de este paquete se recrean
los anillos de llaves gpg correspondientes a los
repositorios:

 - Roraima.
 - Auyantepui.
 - Kerepakupai.
 - Kukenan.
 - Kavac.

Las llaves gpg de los repositorios correspondientes a
la serie 2 de Canaima (2008-2011) han sido
desincorporados.

Se deja igualmente en este repositorio la fuente
primaria de archivos mediante la cual se realiza el
proceso de empaquetamiento-construcción del paquete.
Esta determinación no se considera adecuada y será
revisada en el marco del proceso automatizado de
empaquetamiento de Canaima.

Problemas resueltos en el proceso de empaquetamiento
----------------------------------------------------

Uno de los problemas más relevantes conseguidos en el
proceso fue el manejo de la variable DESTDIR en el
archivo Makefile. Este asunto logró manejarse siguiendo
las recomendaciones de [1]. Sin embargo, no considera
una solución elegante por lo que se continuará una
investigación al respecto.

[1] https://askubuntu.com/questions/473429/debuild-failed-at-dh-install-with-cp-cannot-stat-debian-tmp-path-to-install-b

