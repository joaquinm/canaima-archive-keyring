Algunas consideraciones acerca de canaima-llaves versión 6
----------------------------------------------------------

Luego de estudiar el comportamiento de paquetes como debian-keyring,
debian-archive-keyring y ubuntu-keyring, así como de comprender el
ciclo de vida de las llaves gpg públicas manejadas en Debian, nos
propusimos reescribir este paquete, tomando en cuenta los siguientes
principios:

 - Si bien la nominación de los paquetes no es determinante en la
   funcionalidad de un sistema, sí lo es para el propósito de
   construir la arquitectura de conceptos de una distribución como
   Canaima GNU/Linux. El nombre de paquete canaima-llaves se
   relaciona mucho más con paquetes como debian-keyring y ubuntu-
   keyring que no están asociados a los mecanismos criptográficos
   de los repositorios. En Debian, el paquete que contiene esta
   funcionalidad es debian-archive-keyring, mientras que los
   primeros están relacionados con las llaves gpg que usan los
   desarrolladores para firmar sus paquetes y autenticar sus
   operaciones en la plataforma. En este sentido, renombraremos
   el paquete canaima-llaves que históricamente había servido para
   proveer las llaves gpg de los repositorios de Canaima al nuevo
   identificador canaima-archive-keyring.

 - Está descontinuado el uso de apt-key dentro de los scripts del
   mantenedor. Por esta razón, nos mudamos a la lógica básica que
   provee la herramienta gpg: export, import, keyring, etc. Se
   utilizará igualmente la herramienta jetring-build diseñada
   especialmente para construir anillos gpg escrita por el recordado
   Joey Hess.
 
 - Los archivos de extensión gpg que históricamente se venían
   colocando en /usr/share/keyring han sido mudados de allí. Se han
   renombrado de la manera: <id_llave>.gpg -> <id_llave>.asc. Esto a
   fin de seguir la práctica común de nominación sobre los tipos de
   archivos utilizados por GPG.
 
 - Finalmente, estos archivos asc han sido enviados al directorio
   /usr/share/canaima/gpg-keys creado especialmente para almacenar
   los 

 - Se aprovecha la lógica del ciclo de vida de las llaves gpg que
   permite distinguir entre obsoletas y activas.

 - Simplicidad del paquete y vuelta al método Debian. La reescritura
   de este paquete tomará los principios de debian-archive-keyring.
 
 - La utilidad de este paquete se resume en dos aspectos clave: la
   documentación e intuitividad del ciclo de vida de las llaves pgp,
   y la utilidad práctica de servir como mecanismo de autenticación
   para la gestión de paquetes en Canaima GNU/Linux.

 - La implementación concreta del funcionaiento de este paquete se
   focaliza en el directorio /etc/apt/trusted.d/, ubicación donde
   residen los llaveros consultados por el sistema APT.

TODO
++++

 - Implementar un llavero gpg con las llaves de los desarrolladores.
   Estudiar si podemos implementar al menos dos niveles de autoriza-
   ción: quienes no tiene restricciones en la plataforma, y quienes
   tienen ciertos tipos de restricciones.

 -- Joaquin Muñoz Lucavechi <joaquinm@cnti.gob.ve>, sáb may 12 00:24:11 VET 2018 -0430
